import { useState } from 'react'

export const useForm = (initialState) => {
    const [values, setvalues] = useState(initialState)
    return [values, (formType, formValue) => {
        if (formType === 'reset') {
            return setvalues(initialState)
        }
        return setvalues({ ...values, [formType]: formValue })
    }]
}