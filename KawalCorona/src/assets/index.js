import Logo from './logo.png'
import DummyAvatar from './dummy-avatar.png'
import Facebook from './facebook.png'
import Insagram from './instagram.png'
import Twitter from './twitter.png'

export { Logo, DummyAvatar, Facebook, Insagram, Twitter }