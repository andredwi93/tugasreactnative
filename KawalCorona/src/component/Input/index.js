import React from 'react'
import { StyleSheet, Text, View, TextInput } from 'react-native'

export default function InputText({ label, secureText, value, onChangeText, keyboardType }) {
    return (
        <View>
            <Text style={styles.label}>{label}</Text>
            <TextInput
                style={styles.text}
                secureTextEntry={secureText}
                value={value}
                onChangeText={onChangeText}
                keyboardType={keyboardType} ></TextInput>
        </View>
    )
}

const styles = StyleSheet.create({
    label: {
        fontSize: 16,
        color: '#7D8797',
        marginBottom: 6,
    },
    text: {
        borderWidth: 1,
        borderColor: '#E9E9E9',
        borderRadius: 10,
        padding: 12
    }
})
