import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'

export default function Button({ text, onPress }) {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Text style={styles.text}>{text}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#003366',
        paddingHorizontal: 32,
        paddingVertical: 12,
        borderRadius: 8
    },
    text: {
        color: 'white',
        fontSize: 16,
        textAlign: 'center'
    }
})
