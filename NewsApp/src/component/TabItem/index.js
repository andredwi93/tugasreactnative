import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'

export default function TabItem({ title, active, onPress, onLongPress }) {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress} onLongPress={onLongPress}>
            <Text style={styles.text(active)}>{title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    text: (active) => (
        {
            fontSize: 16,
            fontWeight: '500',
            color: active ? 'white' : '#7D8797'
        }
    )
})
