import React from 'react'
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native'

export default function Loading() {
    return (
        <View style={styles.container}>
            <ActivityIndicator size='large' color="#003366" />
            <Text style={styles.text}>Loading</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,.5)',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 18,
        fontWeight: '600',
        color: '#003366',
        marginTop: 10
    }
})
