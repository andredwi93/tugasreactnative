import firebase from 'firebase'

firebase.initializeApp({
    apiKey: "AIzaSyCVux8QOXPJGqeKmmtVcBrcNyirfb9TRKg",
    authDomain: "newsapp-719ac.firebaseapp.com",
    databaseURL: "https://newsapp-719ac.firebaseio.com",
    projectId: "newsapp-719ac",
    storageBucket: "newsapp-719ac.appspot.com",
    messagingSenderId: "838795929556",
    appId: "1:838795929556:web:f58acdd4ec40677468d841"
})

const Fire = firebase

export default Fire