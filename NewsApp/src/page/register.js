import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { Logo } from '../assets'
import { InputText, Button, Gap } from '../component'
import { ScrollView } from 'react-native-gesture-handler'
import { useForm, storeData } from '../utils'
import { Fire } from '../config'
import { useDispatch } from 'react-redux'

export default function Register({ navigation }) {
    const [form, setForm] = useForm({
        fullName: '',
        email: '',
        password: ''
    })
    const dispatch = useDispatch()

    const onContinue = () => {
        console.log(form)
        dispatch({ type: 'SET_LOADING', value: true })
        Fire.auth().createUserWithEmailAndPassword(form.email, form.password)
            .then(success => {
                const data = {
                    fullName: form.fullName,
                    email: form.email
                }
                Fire.database()
                    .ref(`users/${success.user.uid}/`)
                    .set(data)
                storeData('user', data)
                setForm('reset')
                dispatch({ type: 'SET_LOADING', value: false })
                navigation.navigate('Login')
            })
            .catch(error => {
                dispatch({ type: 'SET_LOADING', value: false })
                alert(error.message)
            })
    }
    return (
        <View style={styles.page}>
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Image source={Logo} style={styles.logo} />
                    <Gap height={24} />
                    <Text style={styles.text}>Belajar bersama Santai Berkualitas</Text>
                    <Gap height={32} />
                    <InputText label="Full Name" value={form.fullName} onChangeText={value => setForm('fullName', value)} />
                    <Gap height={24} />
                    <InputText label="Email Address" value={form.email} keyboardType="email-address" onChangeText={value => setForm('email', value)} />
                    <Gap height={24} />
                    <InputText label="Password" value={form.password} onChangeText={value => setForm('password', value)} secureText />
                    <Gap height={48} />
                    <Button text="Continue" onPress={onContinue} />
                </ScrollView>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: 'white',
        padding: 40,
        justifyContent: 'space-between',
    },
    logo: {
        width: 96,
        height: 85
    },
    text: {
        fontSize: 20,
        color: 'black'
    }
})
