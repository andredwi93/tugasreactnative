import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import { Logo } from '../assets'
import { InputText, Button, Gap } from '../component'
import { ScrollView } from 'react-native-gesture-handler'
import { useForm, storeData } from '../utils'
import { Fire } from '../config'
import { useDispatch } from 'react-redux'

export default function Login({ navigation }) {
    const [form, setForm] = useForm({
        email: '',
        password: ''
    })

    const dispatch = useDispatch()

    const login = () => {
        dispatch({ type: 'SET_LOADING', value: true })
        Fire.auth().signInWithEmailAndPassword(form.email, form.password)
            .then(res => {
                dispatch({ type: 'SET_LOADING', value: false })
                setForm('reset')
                Fire.database().ref(`users/${res.user.uid}/`)
                    .once('value')
                    .then(resDb => {
                        if (resDb.val()) {
                            storeData('user', resDb.val())
                            navigation.replace('MainApp', resDb.val())
                        }
                    })
            })
            .catch(err => {
                alert(err.message)
                dispatch({ type: 'SET_LOADING', value: false })
            })
    }
    return (
        <View style={styles.page}>
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Image source={Logo} style={styles.logo} />
                    <Gap height={24} />
                    <Text style={styles.text}>Belajar bersama Santai Berkualitas</Text>
                    <Gap height={32} />
                    <InputText label="Email Address" value={form.email} keyboardType="email-address" onChangeText={value => setForm('email', value)} />
                    <Gap height={24} />
                    <InputText label="Password" value={form.password} onChangeText={value => setForm('password', value)} secureText />
                    <Gap height={48} />
                    <Button text="Sign In" onPress={login} />
                    <Gap height={24} />
                    <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                        <Text style={styles.create}>Create New Account</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: 'white',
        padding: 40,
        justifyContent: 'space-between',
    },
    logo: {
        width: 96,
        height: 85
    },
    text: {
        fontSize: 20,
        color: 'black'
    },
    create: {
        textDecorationLine: 'underline',
        color: '#7D8797',
        fontSize: 16,
        textAlign: 'center'
    }
})
