import React from 'react'
import { Image, Linking, StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { DummyAvatar, Facebook, Insagram, Twitter } from '../assets'
import { Button, Gap } from '../component'
import { Fire } from '../config'

export default function About({ navigation }) {

    const signOut = () => {
        Fire.auth().signOut().then(() => navigation.replace('Login'))
    }
    return (
        <View style={styles.page}>
            <View>
                <Text style={styles.title}>About Me</Text>
                <View style={styles.avatarWrapper}>
                    <Image style={styles.avatar} source={DummyAvatar} />
                    <View>
                        <Text style={styles.name}>Andriansyah</Text>
                        <Text style={styles.profession}>Programmer</Text>
                    </View>
                </View>
                <Gap height={50} />
                <View style={styles.sosmed}>
                    <Text style={styles.titleSosmed}>Akun Social Media</Text>
                    <View style={styles.imgSosmed}>
                        <TouchableOpacity onPress={() => Linking.openURL('https://www.instagram.com/andre_wardana/')}>
                            <Image source={Insagram} style={styles.icon} />
                        </TouchableOpacity>
                        <Image source={Twitter} style={styles.icon} />
                        <Image source={Facebook} style={styles.icon} />
                    </View>
                    <Gap height={40} />
                    <Text style={styles.titleSosmed}>Portofolio</Text>
                    <Text style={styles.portofolio} onPress={() => Linking.openURL('https://kallatoyota.co.id/')}>https://kallatoyota.co.id/</Text>
                    <Text style={styles.portofolio} onPress={() => Linking.openURL('https://asia.ac.id/')}>https://asia.ac.id/</Text>
                    <Text style={styles.portofolio} onPress={() => Linking.openURL('http://www.teluklamong.co.id/')}>http://www.teluklamong.co.id/</Text>
                    <Text style={styles.portofolio} onPress={() => Linking.openURL('http://altheaecobag.com/')}>http://altheaecobag.com/</Text>
                    <Text style={styles.portofolio} onPress={() => Linking.openURL('http://www.taseco.co.id/')}>http://www.taseco.co.id/</Text>
                    <Text style={styles.portofolio} onPress={() => Linking.openURL('https://www.sajogolaw.com/')}>https://www.sajogolaw.com/</Text>
                </View>
            </View>
            <Button text="Sign Out" onPress={signOut} />
        </View>
    )
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: 'white',
        paddingVertical: 40,
        paddingHorizontal: 24,
        justifyContent: 'space-between'
    },
    title: {
        fontSize: 24,
        fontWeight: '600',
        color: '#003366',
        textAlign: 'center',
        marginBottom: 60
    },
    avatarWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    avatar: {
        width: 60,
        height: 60,
        borderRadius: 60 / 2,
        marginRight: 16
    },
    name: {
        fontSize: 16,
        fontWeight: '600',
        color: 'black'
    },
    profession: {
        fontSize: 12,
        fontWeight: '300',
        color: '#7D8797'
    },
    titleSosmed: {
        fontSize: 16,
        fontWeight: '600',
        color: 'black',
        marginBottom: 20
    },
    imgSosmed: {
        flexDirection: 'row'
    },
    icon: {
        width: 40,
        height: 40,
        borderRadius: 40 / 2,
        marginRight: 16
    },
    portofolio: {
        fontSize: 12,
        color: 'black',
        marginBottom: 10
    }
})
