import React, { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { StyleSheet, Text, View, FlatList, TouchableOpacity } from 'react-native'
import Axios from 'axios'

export default function Home({ navigation }) {
    const [data, setData] = useState({})

    const dispatch = useDispatch()

    useEffect(() => {
        kawalCovid()
        // fetch('https://jsonplaceholder.typicode.com/posts')
        // .then(res => res.json())
        // .then(data => )
    }, [])

    const kawalCovid = async () => {
        dispatch({ type: 'SET_LOADING', value: true })
        try {
            const response = await Axios.get('https://api.kawalcorona.com/indonesia/provinsi/')
            // this.setState({ isError: false, isLoading: false, data: response.data })
            setData(response.data)
            console.log(response.data)
            dispatch({ type: 'SET_LOADING', value: false })
        } catch (error) {
            // this.setState({ isLoading: false, isError: true })
            alert(error.message)
            dispatch({ type: 'SET_LOADING', value: false })
        }
    }

    return (
        <FlatList
            data={data}
            renderItem={({ item }) =>
                <View style={styles.viewList}>
                    {/* <View>
                            <Image source={{ uri: `${item.avatar_url}` }} style={styles.Image} />
                        </View> */}
                    <TouchableOpacity onPress={() => navigation.navigate('Detail', item.attributes)}>
                        <Text style={styles.text}> {item.attributes.Provinsi}</Text>
                        {/* <Text style={styles.textItemUrl}> {item.html_url}</Text> */}

                    </TouchableOpacity>
                </View>
            }
            keyExtractor={({ id }, index) => index}
        />
    )
}

const styles = StyleSheet.create({
    viewList: {
        padding: 16,
        borderWidth: 1,
        borderColor: '#DDD',
    },
    text: {
        fontWeight: 'bold',
        textTransform: 'capitalize',
        marginLeft: 20,
        fontSize: 16
    },
    textItemUrl: {
        fontWeight: 'bold',
        marginLeft: 20,
        fontSize: 12,
        marginTop: 10,
        color: 'blue'
    }
})






// import React, { Component } from 'react'
// import { View, Text, FlatList, StyleSheet, Image, ActivityIndicator } from 'react-native'
// import Axios from 'axios'
// import { TouchableOpacity } from 'react-native-gesture-handler';

// export class Home extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             data: {},
//             isLoading: true,
//             isError: false
//         };
//     }

//     componentDidMount() {
//         this.getGithubUser()
//     }

//     //   Get Api Users
//     getGithubUser = async () => {
//         try {
//             const response = await Axios.get('https://jsonplaceholder.typicode.com/posts')
//             this.setState({ isError: false, isLoading: false, data: response.data })

//         } catch (error) {
//             this.setState({ isLoading: false, isError: true })
//         }
//     }

//     render() {
//         const { navigate } = this.props.navigation
//         //  If load data
//         if (this.state.isLoading) {
//             return (
//                 <View
//                     style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
//                 >
//                     <ActivityIndicator size='large' color='red' />
//                 </View>
//             )
//         }
//         // If data not fetch
//         else if (this.state.isError) {
//             return (
//                 <View
//                     style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
//                 >
//                     <Text>Terjadi Error Saat Memuat Data</Text>
//                 </View>
//             )
//         }
//         // If data finish load
//         return (
//             <FlatList
//                 data={this.state.data}
//                 renderItem={({ item }) =>
//                     <View style={styles.viewList}>
//                         {/* <View>
//                             <Image source={{ uri: `${item.avatar_url}` }} style={styles.Image} />
//                         </View> */}
//                         <TouchableOpacity onPress={() => navigate('Detail', item)}>
//                             <Text style={styles.textItemLogin}> {item.title}</Text>
//                             {/* <Text style={styles.textItemUrl}> {item.html_url}</Text> */}

//                         </TouchableOpacity>
//                     </View>
//                 }
//                 keyExtractor={({ id }, index) => index}
//             />
//         );
//     }
// }

// const styles = StyleSheet.create({
//     viewList: {
//         height: 100,
//         flexDirection: 'row',
//         borderWidth: 1,
//         borderColor: '#DDD',
//         alignItems: 'center'
//     },
//     Image: {
//         width: 88,
//         height: 80,
//         borderRadius: 40
//     },
//     textItemLogin: {
//         fontWeight: 'bold',
//         textTransform: 'capitalize',
//         marginLeft: 20,
//         fontSize: 16
//     },
//     textItemUrl: {
//         fontWeight: 'bold',
//         marginLeft: 20,
//         fontSize: 12,
//         marginTop: 10,
//         color: 'blue'
//     }
// })

// export default Home
