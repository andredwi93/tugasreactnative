import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Gap } from '../component'

export default function Details({ route }) {
    const { Provinsi, Kasus_Meni, Kasus_Posi, Kasus_Semb } = route.params

    return (
        <View style={styles.page}>
            <Text style={styles.prov}>Provinsi: {Provinsi}</Text>
            <Gap height={16} />
            <Text style={styles.text}>Kasus Meninggal: {Kasus_Meni} kasus</Text>
            <Text style={styles.text}>Kasus Positif: {Kasus_Posi} kasus</Text>
            <Text style={styles.text}>Kasus Sembuh: {Kasus_Semb} kasus</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    prov: {
        fontSize: 24,
        fontWeight: 'bold',
    },
    text: {
        fontSize: 16,
        fontWeight: '600'
    }
})
