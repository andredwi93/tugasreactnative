import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import About from '../page/about';
import Login from '../page/login';
import Register from '../page/register';
import BottomTabBar from '../page/bottomTabBar';
import Home from '../page/home';
import Details from '../page/detail';


const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const BottomTab = () => {
    return (
        <Tab.Navigator tabBar={props => <BottomTabBar {...props} />}>
            <Tab.Screen name="Home" component={Home} />
            <Tab.Screen name="About" component={About} />
        </Tab.Navigator>
    )
}

function Route({ navigation }) {
    return (
        <Stack.Navigator initialRouteName="Login">
            <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
            <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
            <Stack.Screen name="MainApp" component={BottomTab} options={{ headerShown: false }} />
            <Stack.Screen name="Detail" component={Details} />
        </Stack.Navigator>
    )
}

export default Route
