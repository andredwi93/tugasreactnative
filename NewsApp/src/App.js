import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import Route from './route'
import { Provider, useSelector } from 'react-redux'
import store from './redux/store'
import Loading from './component/Loading'
import { LogBox } from 'react-native'

const MainApp = () => {
  const stateGlobal = useSelector(state => state)
  LogBox.ignoreLogs(['Warning: Setting a timer'])
  return (
    <>
      <NavigationContainer>
        <Route />
      </NavigationContainer>
      {stateGlobal.loading && <Loading />}
    </>
  )
}

const App = () => {
  return (
    <Provider store={store}>
      <MainApp />
    </Provider>
  )
}

export default App
