import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { colors } from '../../../utils'

export default function Category({ category }) {
    return (
        <View style={styles.category}>
            <Text style={styles.text}>{category}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    category: {
        backgroundColor: colors.secondary,
        borderRadius: 8,
        paddingVertical: 9,
        paddingHorizontal: 8,
    },
    text: {
        fontSize: 12,
        color: colors.text.secondary,
    }
})
