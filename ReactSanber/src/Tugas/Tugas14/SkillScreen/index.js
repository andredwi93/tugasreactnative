import React from 'react'
import { StyleSheet, Text, View, Image, FlatList } from 'react-native'
import { LogoSanber, ILPerson } from '../../../assets'
import { colors } from '../../../utils'
import { Gap } from '../../../components'
import Category from '../Category'
import Skill from '../Skill'

import SkillJSON from './skillData.json'


export default function SkillScreen() {
    return (
        <View style={styles.page}>
            <View style={styles.logoWrapper}>
                <Image source={LogoSanber} style={styles.logo} />
                <Text style={styles.portofolio}>PORTOFOLIO</Text>
            </View>
            <Gap height={3} />
            <View style={styles.container}>
                <View style={styles.profile}>
                    <Image source={ILPerson} style={styles.avatar} />
                    <View>
                        <Text style={styles.intro}>Hi,</Text>
                        <Text style={styles.name}>Mukhlis Hanafi</Text>
                    </View>
                </View>
                <Gap height={16} />
                <Text style={styles.title}>SKILL</Text>
                <View style={styles.categoryWrapper}>
                    <Category category="Library/Framework" />
                    <Category category="Bahasa Pemrograman" />
                    <Category category="Teknologi" />
                </View>
                <FlatList
                    data={SkillJSON.items}
                    renderItem={({ item }) => <Skill
                        key={item.id}
                        iconName={item.iconName}
                        skillName={item.skillName}
                        categoryName={item.categoryName}
                        percentageProgress={item.percentageProgress} />}
                    keyExtractor={item => item.id.toString()}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: 40 }} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: 'white',
    },
    logoWrapper: {
        alignItems: 'flex-end'
    },
    logo: {
        width: 187,
        height: 51,
    },
    portofolio: {
        fontSize: 12,
        color: colors.primary,
        paddingRight: 16
    },
    container: {
        paddingHorizontal: 16,
        flex: 1,
    },
    profile: {
        flexDirection: 'row',
    },
    avatar: {
        width: 32,
        height: 32,
        borderRadius: 32 / 2,
        marginRight: 11
    },
    intro: {
        fontSize: 12,
        color: 'black'
    },
    name: {
        fontSize: 16,
        color: colors.text.secondary
    },
    title: {
        fontSize: 36,
        color: colors.text.secondary,
        borderBottomWidth: 4,
        borderBottomColor: colors.primary
    },
    categoryWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 10
    }
})
