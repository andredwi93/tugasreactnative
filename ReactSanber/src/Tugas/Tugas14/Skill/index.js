import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { colors } from '../../../utils'

export default function Skill({ skillName, categoryName, iconName, percentageProgress }) {
    return (
        <View style={styles.container}>
            <Icon name={iconName} size={70} />
            <View style={styles.skillWrapper}>
                <Text style={styles.skill}>{skillName}</Text>
                <Text style={styles.category}>{categoryName}</Text>
                <Text style={styles.precentage}>{percentageProgress}</Text>
            </View>
            <Icon name="chevron-right" size={60} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.secondary,
        flexDirection: 'row',
        borderRadius: 8,
        elevation: 4,
        paddingHorizontal: 10,
        paddingVertical: 13,
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 8
    },
    skillWrapper: {
        flex: 1,
        marginLeft: 24
    },
    skill: {
        fontWeight: 'bold',
        fontSize: 24,
        color: colors.text.secondary,
    },
    category: {
        fontWeight: 'bold',
        fontSize: 16,
        color: colors.text.primary
    },
    precentage: {
        fontWeight: 'bold',
        fontSize: 48,
        color: 'white',
        textAlign: 'center'
    }
})
