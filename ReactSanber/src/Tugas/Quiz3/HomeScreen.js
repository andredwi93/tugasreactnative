import React, { useState } from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';
import Login from './LoginScreen'
import data from './data.json';

const DEVICE = Dimensions.get('window')

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
      totalPrice: 0,
    }
  }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  updatePrice(price) {
    //? #Soal Bonus (10 poin) 
    //? Buatlah teks 'Total Harga' yang akan bertambah setiap kali salah satu barang/item di klik/tekan.
    //? Di sini, buat fungsi untuk menambahkan nilai dari state.totalPrice dan ditampilkan pada 'Total Harga'.

    // Kode di sini    
    this.setState({
      totalPrice: price
    })
  }


  render() {
    const { navigate } = this.props.navigation
    return (
      <View style={styles.container}>
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>Hai,{'\n'}
              {/* //? #Soal 1 Tambahan, Simpan userName yang dikirim dari halaman Login pada komponen Text di bawah ini */}
              <Text style={styles.headerText}>{navigate.name}</Text>
            </Text>

            {/* //? #Soal Bonus, simpan Total Harga dan state.totalPrice di komponen Text di bawah ini */}
            <Text style={{ textAlign: 'right' }}>Total Harga{'\n'}
              <Text style={styles.headerText}>{this.state.totalPrice}</Text>
            </Text>
          </View>
          <View>

          </View>
          <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 }}
            placeholder='Cari barang..'
            onChangeText={(searchText => this.setState({ searchText }))}
          />
        </View>

        {/* 
        //? #Soal No 2 (15 poin)
        //? Buatlah 1 komponen FlatList dengan input berasal dari data.json
        //? dan pada prop renderItem menggunakan komponen ListItem -- ada di bawah --
        //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal)

        // Lanjutkan di bawah ini!
        */}
        {
          <FlatList
            data={data.produk}
            renderItem={({ item }) => <ListItem
              key={item.id}
              gambaruri={item.gambaruri}
              nama={item.nama}
              harga={item.harga}
              stock={item.stock} />}
            keyExtractor={item => item.id.toString()}
            showsVerticalScrollIndicator={false}
            numColumns={2}
          ></FlatList>
        }

      </View>
    )
  }
};


const ListItem = ({ gambaruri, nama, harga, stock }) => {
  let currencyFormat = (num) => {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };


  //   //? #Soal No 3 (15 poin)
  //   //? Buatlah styling komponen ListItem, agar dapat tampil dengan baik di device
  return (
    <View style={styles.itemContainer}>
      <Image source={{ uri: gambaruri }} style={styles.itemImage} resizeMode='contain' />
      <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{nama}</Text>
      <Text style={styles.itemPrice}>{currencyFormat(Number(harga))}</Text>
      <Text style={styles.itemStock}>Sisa stok: {stock}</Text>
      <Button title='BELI' color='blue' style={styles.buttonText} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    width: DEVICE.width * 0.44,
    margin: 10
  },
  itemImage: {
    width: '100%',
    height: 60,
    marginBottom: 10
  },
  itemName: {
    marginBottom: 8
  },
  itemPrice: {
    marginBottom: 8
  },
  itemStock: {
    marginBottom: 10
  },
  itemButton: {
  },
  buttonText: {
  }
})
