import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { ILDummyAvatar, IconTwitter, IconFacebook, IconInstagram } from '../../../assets'
import { Gap } from '../../../components'

export default function About() {
    return (
        <View style={styles.page}>
            <Text style={styles.title}>About Us</Text>
            <Gap height={60} />
            <View style={styles.profile}>
                <Image source={ILDummyAvatar} style={styles.avatar} />
                <View style={styles.professionWrapper}>
                    <Text style={styles.name}>Andriansyah</Text>
                    <Text style={styles.profession}>FrontEnd Developer</Text>
                </View>
            </View>
            <Gap height={50} />
            <View>
                <Text style={styles.text}>Akun Sosial Media</Text>
                <View style={styles.sosmed}>
                    <Image source={IconTwitter} style={styles.icon} />
                    <Image source={IconFacebook} style={styles.icon} />
                    <Image source={IconInstagram} style={styles.icon} />
                </View>
            </View>
            <Gap height={40} />
            <View>
                <Text style={styles.text}>Portofolio</Text>
                <Text style={styles.link}>https://blog.sanbercode.com</Text>
                <Gap height={10} />
                <Text style={styles.link}>https://about.gitlab.com/</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        paddingVertical: 40,
        paddingHorizontal: 24,
        backgroundColor: '#fff'
    },
    title: {
        fontWeight: '600',
        fontSize: 24,
        lineHeight: 33,
        color: '#003366',
        textAlign: 'center'
    },
    profile: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    avatar: {
        width: 60,
        height: 60,
        borderRadius: 60 / 2
    },
    professionWrapper: {
        flex: 1,
        marginLeft: 16
    },
    name: {
        fontWeight: '600',
        fontSize: 16,
        lineHeight: 22,
        color: 'black',
        marginBottom: 4
    },
    profession: {
        fontWeight: '300',
        fontSize: 12,
        lineHeight: 16,
        color: '#7D8797'
    },
    text: {
        fontWeight: '600',
        fontSize: 16,
        lineHeight: 22,
        color: 'black',
        marginBottom: 20
    },
    sosmed: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    icon: {
        width: 40,
        height: 40,
        borderRadius: 40 / 2,
        marginRight: 16
    },
    link: {
        fontWeight: '400',
        fontSize: 12,
        lineHeight: 16,
        color: '#000000',
        textDecorationLine: 'underline'
    }
})
