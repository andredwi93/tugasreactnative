import React from 'react'
import { Image, StyleSheet, Text, View, TextInput } from 'react-native'
import { ILLogo } from '../../../assets/Ilustration'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Gap } from '../../../components'

export default function Login({ navigation }) {
    return (
        <View style={styles.page}>
            <View>
                <Image source={ILLogo} style={styles.logo} />
                <Text style={styles.title}>Belajar bersama Santai Berkualitas</Text>
                <View style={styles.inputWrapper}>
                    <Text style={styles.text}>Email Address</Text>
                    <TextInput style={styles.input}></TextInput>
                </View>
                <Gap height={24} />
                <View style={styles.inputWrapper}>
                    <Text style={styles.text}>Password</Text>
                    <TextInput style={styles.input} secureTextEntry={true}></TextInput>
                </View>
                <Gap height={24} />
                <View style={styles.inputWrapper}>
                    <Text style={styles.text}>Confirm Password</Text>
                    <TextInput style={styles.input} secureTextEntry={true}></TextInput>
                </View>
            </View>
            <View>
                <TouchableOpacity style={styles.signIn} onPress={() => navigation.openDrawer()}>
                    <Text style={styles.textSignIn}>Sign In</Text>
                </TouchableOpacity>
                <Gap height={16} />
                <TouchableOpacity style={styles.newAccount}>
                    <Text style={styles.textAccount}>Create New Account</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        padding: 40,
        backgroundColor: 'white',
        justifyContent: 'space-between'
    },
    logo: {
        width: 96,
        height: 85
    },
    text: {
        fontSize: 16,
        lineHeight: 22,
        color: '#7D8797',
        marginBottom: 6
    },
    input: {
        borderWidth: 1,
        borderColor: '#E9E9E9',
        height: 40,
        borderRadius: 8
    },
    title: {
        maxWidth: 194,
        fontWeight: '600',
        fontSize: 20,
        lineHeight: 27,
        color: '#000000',
        marginTop: 24,
        marginBottom: 32
    },
    signIn: {
        backgroundColor: '#003366',
        borderRadius: 8,
        height: 40,
        paddingVertical: 9
    },
    textSignIn: {
        fontWeight: '600',
        fontSize: 16,
        lineHeight: 22,
        color: '#ffffff',
        textAlign: "center"
    },
    textAccount: {
        fontWeight: '400',
        fontSize: 16,
        lineHeight: 22,
        textDecorationLine: 'underline',
        color: '#7D8797',
        textAlign: 'center'
    }
})
