import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export default function AddScreen() {
    return (
        <View style={styles.page}>
            <Text>Add Screen</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
