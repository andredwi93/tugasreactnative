import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList } from 'react-native'
import { Logo } from '../../assets'
import Icon from 'react-native-vector-icons/MaterialIcons'
import VideoItem from '../../components/videoItem';
import data from '../../data.json';

export default function Tugas12() {
    return (
        <View style={styles.page}>
            <View style={styles.navBar}>
                <Image source={Logo} style={styles.logo} />
                <View style={styles.rigthNav}>
                    <TouchableOpacity>
                        <Icon name="search" size={25} style={styles.navItem} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon name="account-circle" size={25} style={styles.navItem} />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.body}>
                <FlatList
                    data={data.items}
                    renderItem={(video) => <VideoItem video={video.item} />}
                    keyExtractor={(item) => item.id}
                    ItemSeparatorComponent={() => <View style={{ height: 0.5, backgroundColor: '#E5E5E5' }} />}
                />
            </View>

            <View style={styles.tabBar}>
                <TouchableOpacity style={styles.tabItem}>
                    <Icon name="home" size={25} />
                    <Text style={styles.tabTitle}>Home</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabItem}>
                    <Icon name="whatshot" size={25} />
                    <Text style={styles.tabTitle}>Trending</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabItem}>
                    <Icon name="subscriptions" size={25} />
                    <Text style={styles.tabTitle}>Subscriptions</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabItem}>
                    <Icon name="folder" size={25} />
                    <Text style={styles.tabTitle}>Library</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    page: {
        flex: 1
    },
    navBar: {
        height: 55,
        backgroundColor: 'white',
        elevation: 3,
        paddingHorizontal: 16,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    logo: {
        width: 98,
        height: 22
    },
    rigthNav: {
        flexDirection: 'row',
    },
    navItem: {
        marginLeft: 24
    },
    tabBar: {
        backgroundColor: 'white',
        height: 60,
        borderTopWidth: 0.5,
        borderColor: '#E5E5E5',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    body: {
        flex: 1
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabTitle: {
        fontSize: 11,
        color: '#3c3c3c',
        paddingTop: 4
    }
})
