import React from 'react'

import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Login, About, Main, SkillScreen, Home, BottomNavBar, ProjectScreen, AddScreen, LoginScreen, HomeScreen } from '../Tugas';
import Icon from 'react-native-vector-icons/Ionicons'


// const HomeStack = createStackNavigator();
// const LoginStack = createStackNavigator();
// const AboutStack = createStackNavigator();
// const Drawer = createDrawerNavigator();
// const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

// const HomeStackScreen = ({ navigation }) => {
//     return (
//         <HomeStack.Navigator screenOptions={{
//             headerTitleAlign: {
//                 textAlign: 'center'
//             }
//         }}>
//             <HomeStack.Screen name="Home" component={Home} options={{
//                 title: 'Home', headerLeft: () => (
//                     <Icon.Button name="ios-menu" size={25} backgroundColor='blue' onPress={() => { navigation.openDrawer() }} ></Icon.Button>
//                 )
//             }} />
//         </HomeStack.Navigator>
//     )
// }

// const AboutStackScreen = ({ navigation }) => {
//     return (
//         <AboutStack.Navigator screenOptions={{
//             headerTitleAlign: {
//                 textAlign: 'center'
//             }
//         }}  >
//             <AboutStack.Screen name="About" component={About} options={{
//                 headerLeft: () => (
//                     <Icon.Button name="ios-menu" size={25} backgroundColor='blue' onPress={() => { navigation.openDrawer() }} ></Icon.Button>
//                 )
//             }} />
//         </AboutStack.Navigator>
//     )
// }

// const BottomTab = () => {
//     return (
//         <Tab.Navigator tabBar={props => <BottomNavBar {...props} />}>
//             <Tab.Screen name="Skill Screen" component={SkillScreen} />
//             <Tab.Screen name="Project Screen" component={ProjectScreen} />
//             <Tab.Screen name="Add Screen" component={AddScreen} />
//         </Tab.Navigator>        
//     )
// }

const Router = () => {
    return (
        // <Drawer.Navigator initialRouteName="Login">
        //     <Drawer.Screen name="Login" component={Login} />
        //     <Drawer.Screen name="Home" component={HomeStackScreen} />
        //     <Drawer.Screen name="BottomTab" component={BottomTab} />
        //     <Drawer.Screen name="About" component={AboutStackScreen} />
        // </Drawer.Navigator>
        <Stack.Navigator initialRouteName="Login" >
            <Stack.Screen name='Login' component={LoginScreen} options={{ headerShown: false }} />
            <Stack.Screen name='Home' component={HomeScreen} options={{ headerTitle: 'Daftar Barang' }} />
        </Stack.Navigator>
    )
}

export default Router