const mainColors = {
    blue1: '#3EC6FF',
    blue2: '#B4E9FF',
    darkBlue: '#003366'
}

export const colors = {
    primary: mainColors.blue1,
    secondary: mainColors.blue2,
    text: {
        primary: mainColors.blue1,
        secondary: mainColors.darkBlue
    }
}