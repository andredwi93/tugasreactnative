import ILDummyAvatar from './dummy-avatar.png'
import ILLogo from './logo.png'
import ILPerson from './person-circle.png'

export { ILDummyAvatar, ILLogo, ILPerson }