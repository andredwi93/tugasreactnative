import IconFacebook from './facebook.png'
import IconInstagram from './instagram.png'
import IconTwitter from './twitter.png'

export { IconFacebook, IconInstagram, IconTwitter }